def calculate_score_video_cache_pairs(number_of_caches , number_of_videos , number_of_requests , endpoints_connected_to_cache):
    score = []
    #score[i , j] is the score for video i going to cache j
    for i in xrange(0 , number_of_caches):
        l = []
        for j in xrange(0 , number_of_videos):
            l.append(0)
        score.append(l)

    for i in xrange(0 , number_of_requests):
        vals = requests[i]
        video = int(vals[0])
        endpoint = int(vals[1])
        number_of_requests = int(vals[2])
        #now all that matters are the caches connected to this endpoint
        #this is basic heuristic to find out which cache to assign to a videp
        print requests[i]
        for j in endpoints_connected_to_cache[endpoint]:
            score[j][video] += number_of_requests
    print "Score for each cache and video pair vs number of requests ", score # Each entry in the array corresponds to a particular cache, where each entry is of length -> number of videos. 



number_of_videos, number_of_endpoints, number_of_requests, number_of_caches, cache_size = map(int,raw_input().split())
video_sizes = map(int,raw_input().split())
latency_of_endpoint_to_datacenter = {}
latency_of_endpoint_to_cache = {}
cache_connected_to_endpoints = {}
endpoints_connected_to_cache = {}
for i in xrange(number_of_endpoints):
    latency,connected_to_caches = map(int,raw_input().split())
    latency_of_endpoint_to_datacenter[i]=latency    
    endpoints_connected_to_cache[i] = []
    for j in xrange(connected_to_caches):
        cache, latency_to_cache = map(int,raw_input().split())
        endpoints_connected_to_cache[i].append(cache)
        latency_of_endpoint_to_cache[(i,cache)] = latency_to_cache
        if cache not in cache_connected_to_endpoints:
            cache_connected_to_endpoints[cache]=[]
        cache_connected_to_endpoints[cache].append(i)

requests = []
number_of_times_video_is_requested = {}
#total_video_requests = {}
for i in xrange(number_of_requests):
    vals = raw_input().split()
    requests.append(list(vals))         #(video , endpoint , number_of_requests)
    if vals[0] not in number_of_times_video_is_requested:
        number_of_times_video_is_requested[vals[0]]=[]
    number_of_times_video_is_requested[vals[0]].append(vals[1:])

calculate_score_video_cache_pairs(number_of_caches , number_of_videos , number_of_requests , endpoints_connected_to_cache)





print "Latency of endpoint to cache\n",latency_of_endpoint_to_cache
print "Latency of endpoint to datacenter\n", latency_of_endpoint_to_datacenter
print "Number of videos ", number_of_videos
print "Number of endpoints ", number_of_endpoints
print "Number of requests ", number_of_requests
print "Number of caches ", number_of_caches
print "Cache size is ", cache_size
print "Video Sizes are ", video_sizes
print "Video requests ", number_of_times_video_is_requested
print "Caches connected to what endpoints ", cache_connected_to_endpoints
print "endpoints connected to cache " , endpoints_connected_to_cache
